import { renderComponent , expect } from '../test_helper';
import DeviceList from '../../src/containers/device-list';

describe('DeviceList' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(DeviceList);
  });

  it('renders DeviceList', () => {
    expect(component).to.exist;
  });
});
