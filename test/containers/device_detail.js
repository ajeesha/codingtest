import { renderComponent , expect } from '../test_helper';
import DeviceDetail from '../../src/containers/device-detail';

describe('DeviceDetail' , () => {
  let component;

  beforeEach(() => {
    component = renderComponent(DeviceDetail);
  });

  it('renders DeviceDetail', () => {
    expect(component).to.exist;
  });
});
