### Getting Started
Download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> npm install
> npm start
> open chrome browser and check with http://localhost:8080
```
