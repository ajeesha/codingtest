import React, { Component } from 'react';
import DeviceList from '../containers/device-list'
import DeviceDetail from '../containers/device-detail'

export default class App extends Component {
  render() {
    return (
      <div>
      	<DeviceList />
      	<DeviceDetail />
      </div>
    );
  }
}
