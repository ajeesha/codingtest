function selectDevice(device) {
  return {
    type: 'DEVICE_SELECTED',
    payload: device
  }
}
export default selectDevice;
