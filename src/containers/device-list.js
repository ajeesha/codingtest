import React, {Component} from 'react';
import {connect} from 'react-redux';
import selectDevice from '../actions/action_select_device';
import {bindActionCreators} from 'redux';

class DeviceList extends Component {
  renderList() {
    return this.props.devices.map((device) => {
      return (
        <li
        key={device.id}
        onClick={() => this.props.selectDevice(device)}
        className='list-group-item device_name'><a>{device.name}</a></li>
      );
    });
  }
  render() {
    return (
      <ul className = 'list-group col-sm-4'>
        {this.renderList()}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  return {
    devices: state.devices
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({selectDevice: selectDevice}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DeviceList)
