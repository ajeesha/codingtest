import React, { Component } from 'react';
import {connect} from 'react-redux';

class DeviceDetail extends Component {
	render() {
		if (!this.props.device) {
			return (
				<div>Click one of the devices to see details.</div>
			);
		}
		return (
			<div>
				<h3> Details for: {this.props.device.name}</h3>
				<div>Operating System: {this.props.device.os}</div>
				<div>Manufacturer: {this.props.device.manufacturer}</div>
				<div>lastCheckedOutDate: {this.props.device.lastCheckedOutDate}</div>
				<div>lastCheckedOutBy: {this.props.device.lastCheckedOutBy}</div>
				<div>isCheckedOut: {this.props.device.isCheckedOut ? 'Yes': 'No'}</div>				
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
    device: state.activeDevice
  };
}

export default connect(mapStateToProps)(DeviceDetail)
