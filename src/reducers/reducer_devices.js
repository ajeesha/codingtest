export default function () {
	return [{
		"id": 1,
		"name": "Moto G",
		"os": "Android 4.3",
		"manufacturer": "Motorola",
		"lastCheckedOutDate": "2016-02-21T09:10:00--05:00",
		"lastCheckedOutBy": "Ian",
		"isCheckedOut": false,
	},{
		"id": 2,
		"name": "Nokia M7",
		"os": "Android 4.3",
		"manufacturer": "Nokia",
		"lastCheckedOutDate": "2021-01-12T09:10:00--08:00",
		"lastCheckedOutBy": "John",
		"isCheckedOut": true,
	},{
		"id": 3,
		"name": "Iphone 6.0",
		"os": "IOS 8.0",
		"manufacturer": "Apple",
		"lastCheckedOutDate": "2020-12-25T09:10:00--03:00",
		"lastCheckedOutBy": "Philip",
		"isCheckedOut": false,
	}];

}