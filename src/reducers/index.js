import { combineReducers } from 'redux';
import DevicesReducer from './reducer_devices'
import ActiveDeviceReducer from './reducer_active_device'

const rootReducer = combineReducers({
	devices: DevicesReducer,
	activeDevice: ActiveDeviceReducer
});

export default rootReducer;
